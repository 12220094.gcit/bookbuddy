function updateBook(r) {
    selectedRow = r.parentElement.parentElement;
    // console.log(selectedRow)
  
    document.getElementById("id").value = selectedRow.cells[0].innerHTML;
    document.getElementById("title").value = selectedRow.cells[1].innerHTML;
    document.getElementById("author").value = selectedRow.cells[2].innerHTML;
    document.getElementById("cost").value = selectedRow.cells[3].innerHTML;
    document.getElementById("quantity").value = selectedRow.cells[4].innerHTML;

    var btn = document.getElementById("button-add");
  
    cid = selectedRow.cells[0].innerHTML;
  
    if (btn) {
        btn.innerHTML = "Update";
        btn.setAttribute("onclick" , "update(id)");
    }
  }
  
  function update(bookid) {
    var newData = getFormData()
    fetch('/addbook/'+bookid,{
        method : "PUT",
        body : JSON.stringify(newData),
        headers : {"Content-type" : "application/json; charset=UTF-8"}
  
    }).then(res => {
        if(res.ok){
            selectedRow.cells[0].innerHTML = newData.bookid;
            selectedRow.cells[1].innerHTML = newData.title;
            selectedRow.cells[2].innerHTML = newData.author;
            selectedRow.cells[3].innerHTML = newData.cost;
            selectedRow.cells[4].innerHTML = newData.quantity;


            var button = document.getElementById("button-add");
            button.innerHTML = "ADD";
            button.setAttribute("onclick", "addBook()");
  
            selectedRow = null;
            resetform();
  
        }else {
            alert("Server : Update request error")
        }
  
    })
  }
  
  function deleteBook(r) {
    if (confirm('Are you sure you want to DELETE this?')) {
        selectedRow = r.parentElement.parentElement;
        bookid = selectedRow.cells[0].innerHTML;
  
        fetch('/book/'+bookid,{
            method:"DELETE",
            headers : {"Content-type" : "application/json; charset=UTF-8"}
        });
  
        var rowIndex = selectedRow.rowIndex;
        if (rowIndex > 0) {
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;
  
      }
  }