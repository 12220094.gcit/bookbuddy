function signUp() {
    var adminData = {
        fullname : document.getElementById("fullname").value ,
        email : document.getElementById("email").value,
        password : document.getElementById("pw1").value,
        pw : document.getElementById("pw2").value,
    }
    if (adminData.password !== adminData.pw) {
        alert("PASSWORD doesn't match!")
        return
    }
    console.log(adminData)
    fetch('/signup', {
        method : "POST",
        body :JSON.stringify(adminData),
        headers: {"content-type" : "application/json; charset=UTF-8"}
    })
    .then((response) => {
        if (response.ok) {
          console.log("Redirecting to login page");
          window.open("login.html", "_self");
        } else {
          throw new Error("Network response was not ok.");
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
}
var submit = document.querySelector("#submit-btn")
submit.addEventListener("click", (e) =>{
  e.preventDefault()
  console.log("here")
  signUp()
})

console.log("ADMIN TEST")