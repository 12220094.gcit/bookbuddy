
function showBooks(data) {
    const books = JSON.parse(data)
    books.forEach(book => {
        newRow(book);
    });}

window.onload = function() {
    fetch('/books')
    .then(response => response.text())
    .then(data => showBooks(data));
}

function newRow(book){
    console.log(book)
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.length);
    
    var td = []
    for (i = 0; i < table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }
    td[0].innerHTML = book.bookid;
    td[1].innerHTML = book.title;
    td[2].innerHTML = book.author;
    td[3].innerHTML = book.cost;
    td[4].innerHTML = book.quantity;
    td[5].innerHTML = '<td class = "btn"><Button onclick = updateBook(this) class= "update">Update</Button></td>';
    td[6].innerHTML = `<td class = "btn"><Button onclick = deleteBook(this) class="delete">Delete</Button></td>`;
}
var selectedRow
function deleteBook(r) {
    console.log("FUNCION TEST")
    if (confirm('Are you sure you want to DELETE this?')) {
        selectedRow = r.parentElement.parentElement;
        bookid = selectedRow.cells[0].innerHTML;
        fetch('/deletebook/'+bookid,{
            method:"DELETE",
            headers : {"Content-type" : "application/json; charset=UTF-8"}
        });

        var rowIndex = selectedRow.rowIndex;
        if (rowIndex > 0) {
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;

    }

}

function updateBook(r){
    popup = document.getElementById("popupToggle")
    popup.checked = !popup.checked;
    selectedRow = r.parentElement.parentElement;
    bookid = selectedRow.cells[0].innerHTML
    // Adding in the book values to the fields
    document.getElementById("bookidForm").value = selectedRow.cells[0].innerHTML
    document.getElementById("bookTitleForm").value = selectedRow.cells[1].innerHTML
    document.getElementById("authorNameForm").value = selectedRow.cells[2].innerHTML
    document.getElementById("CostForm").value = selectedRow.cells[3].innerHTML
    document.getElementById("QuantityForm").value = selectedRow.cells[4].innerHTML


    const submitBtn = document.getElementById("submitBtn")
    submitBtn.addEventListener("click", (e) => {
        e.preventDefault()
        var data = getFormData()

        fetch('/updatebook/'+bookid,{
            method:"PUT",
            body: JSON.stringify(data),
            headers : {"Content-type" : "application/json; charset=UTF-8"}
        }).then(response1 => {
            window.location.reload();
                }).catch(e => {
            alert(e)
        })
        
        var rowIndex = selectedRow.rowIndex;
        if (rowIndex > 0) {
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;
    

    })

    
}

function getFormData() {
    var formData = {
        bookid : parseInt(document.getElementById('bookidForm').value),
        title : document.getElementById('bookTitleForm').value,
        author : document.getElementById('authorNameForm').value,
        cost : parseInt(document.getElementById('CostForm').value),
        quantity : parseInt(document.getElementById('QuantityForm').value)
    }
    return formData;
}