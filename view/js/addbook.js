const bookBtn = document.querySelector("#addBookBtn")
bookBtn.addEventListener('click', (e) => {
    e.preventDefault()
    addBook()
})
function resetform() {
    document.getElementById("bookid").value = "";
    document.getElementById("title").value = "";
    document.getElementById("author").value = "";
    document.getElementById("cost").value = "";
    document.getElementById("quantity").value = "";

}


window.onload = function() {
    fetch('/books')
        .then(response => response.text())
        .then(data => showBooks(data));
}  

function getFormData() {
    var formData = {
        bookid : parseInt(document.getElementById('bookid').value),
        title : document.getElementById('title').value,
        author : document.getElementById('author').value,
        cost : parseInt(document.getElementById('cost').value),
        quantity : parseInt(document.getElementById('quantity').value)
    }
    console.log(formData)
    return formData;
}

function addBook() {
    var data = getFormData()
    var sid  = data.bookid;

    fetch("/addbook" , {
        method : "POST",
        body: JSON.stringify(data),
        headers: {"Content-type": "application/json; charset=UTF-8"}

    }).then(response1 => {
        console.log("HERE")
        if (response1.ok) {
            fetch('/searchbook/'+sid)
            .then(response2 => response2.text())
            .then(data => showBook(data))
        } else {
            throw new Error(response1.statusText)
        }
    }).catch(e => {
        alert(e)
    })
    resetform();

}

function newRow(book) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.length);

    var td = []
    for (i = 0; i < table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }
    td[0].innerHTML = book.bookid;
    td[1].innerHTML = book.title;
    td[2].innerHTML = book.author;
    td[3].innerHTML = book.cost;
    td[4].innerHTML = book.quantity;
    td[5].innerHTML = '<input type="button" onclick="deleteStudent(this)" value="delete" id="button-1">';
    td[6].innerHTML = '<input type="button" onclick="updateStudent(this)" value="edit" id="button-2">';
}

function showBook(data) {
    const book = JSON.parse(data);
    newRow(book)
}

function showBooks(data) {
    const books = JSON.parse(data)
    books.forEach(ebook => {
        newRow(ebook);
    });
}

var selectedRow = null

function updateBook(r) {
    selectedRow = r.parentElement.parentElement;
    // console.log(selectedRow)

}

function updateBooks(r) {
    selectedRow = r.parentElement.parentElement;
    // console.log(selectedRow)

    document.getElementById("bookid").value = selectedRow.cells[0].innerHTML;
    document.getElementById("title").value = selectedRow.cells[1].innerHTML;
    document.getElementById("author").value = selectedRow.cells[2].innerHTML;
    document.getElementById("cost").value = selectedRow.cells[3].innerHTML;
    document.getElementById("quantity").value = selectedRow.cells[4].innerHTML;

    var btn = document.getElementById("button-add");

    sid = selectedRow.cells[0].innerHTML;

    if (btn) {
        btn.innerHTML = "Update";
        btn.setAttribute("onclick" , "update(bookid)");
    }
}
function update(bookid) {
    var newData = getFormData()
    fetch('/books/'+bookid,{
        method : "PUT",
        body : JSON.stringify(newData),
        headers : {"Content-type" : "application/json; charset=UTF-8"}

    }).then(res => {
        if(res.ok){
            selectedRow.cells[0].innerHTML = newData.bookid;
            selectedRow.cells[1].innerHTML = newData.title;
            selectedRow.cells[2].innerHTML = newData.author;
            selectedRow.cells[3].innerHTML = newData.cost;
            selectedRow.cells[4].innerHTML = newData.quantity;


            var button = document.getElementById("button-add");
            button.innerHTML = "ADD";
            button.setAttribute("onclick", "addBook()");

            selectedRow = null;
            resetform();

        }else {
            alert("Server : Update request error")
        }

    })
}

function deleteBook(r) {
    if (confirm('Are you sure you want to DELETE this?')) {
        selectedRow = r.parentElement.parentElement;
        sid = selectedRow.cells[0].innerHTML;

        fetch('/book/'+bookid,{
            method:"DELETE",
            headers : {"Content-type" : "application/json; charset=UTF-8"}
        });

        var rowIndex = selectedRow.rowIndex;
        if (rowIndex > 0) {
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;

    }
}