

function login() {
    console.log("FUNCTION TEST")
    var loginData = {
        email: document.getElementById("email").value,
        password: document.getElementById("pw").value
    };

    fetch('/login', {
        method: "POST",
        body: JSON.stringify(loginData),
        headers: { "content-type": "application/json; charset=UTF-8" }
    })
    .then(response => {
        if (response.ok) {
            window.location.href = "Admindash.html"; 
        } else {
            throw new Error(response.statusText);
        }
    })
    .catch(error => {
        if (error.message === "Unauthorized") {
            alert("Credentials do not match!");
        } else {
            console.error("An error occurred:", error);
        }
    });
}
var loginBtn =  document.querySelector("#login-btn")
console.log(loginBtn)
// loginBtn.addEventListener("click", (e) => {
//     console.log("BUTTON TEST")
//     e.preventDefault()
//     login()
// })
loginBtn.addEventListener('click', (e)=>{
    e.preventDefault()
    login()
})

