package main

import (
	"bookApp/routes"
)

func main() {
	// router := mux.NewRouter()
	// router.HandleFunc("/home", homeHandler)
	// err := http.ListenAndServe(":8081", router)
	// if err != nil {
	// 	return
	// }
	routes.InitializeRoutes()
}

// func homeHandler(w http.ResponseWriter, r *http.Request) {
// 	_, err := w.Write([]byte("hello world"))
// 	if err != nil {
// 		fmt.Println("error: ", err)
// 	}
// }
