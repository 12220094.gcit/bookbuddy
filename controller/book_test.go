package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddBook(t *testing.T) {
	url := "http://localhost:8081/addbook"

	var jsonStr = []byte(`{"bookid":10, "title": "color", "author":"kC", "cost": 120, "quantity": 12}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	// cookie, err := resp.Cookie("my-cookie")
	if err != nil {
		panic(err)
	}
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status":"book added successfully"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestBookNotFound(t *testing.T) {
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:8081/searchbook/12302")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"book not found"}`
	assert.JSONEq(expResp, string(body))
}

func TestGetBook(t *testing.T) {
	c := http.Client{}
	r, _ := c.Get("http://localhost:8081/addbook/3252")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{"bookid":3252, "title": "book", "author":"Anna", "cost": 124, "quantity": 1}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeleteBook(t *testing.T) {
	url := "http://localhost:8080/searchbook/103"

	req, _ := http.NewRequest("DELETE", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status":"deleted"}`
	assert.JSONEq(t, expResp, string(body))

}
