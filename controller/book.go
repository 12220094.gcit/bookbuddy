package controller

import (
	"bookApp/Model"
	"bookApp/utils/httpResp"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Add book
func Addbook(w http.ResponseWriter, r *http.Request) {
	var book Model.Book
	_ = book

	decoder := json.NewDecoder(r.Body)
	// fmt.Println(decoder)

	if err := decoder.Decode(&book); err != nil {
		// w.Write([]byte("Invalid json data"))
		response, _ := json.Marshal(map[string]string{"error": "Invalid Json Type"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	defer r.Body.Close()

	saveErr := book.Create()
	fmt.Println(saveErr)
	if saveErr != nil {
		// w.Write([]byte("Database error"))
		response, _ := json.Marshal(map[string]string{"error": saveErr.Error()})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}
	//no error
	// w.Write([]byte("response success"))
	response, _ := json.Marshal(map[string]string{"status": "book added successfully"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

// Search book
func GetBook(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GETBOOK TEST")
	bid := mux.Vars(r)["bid"]
	fmt.Println(bid)
	bookId, idErr := getBookId(bid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := Model.Book{ID: int(bookId)}
	getErr := s.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "book not found")
		default:
			httpResp.RespondWithError(w, http.StatusNotFound, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, s)
}

func getBookId(userIdParam string) (int64, error) {
	fmt.Println(userIdParam)
	userId, userErr := strconv.ParseInt(userIdParam, 10, 64)
	if userErr != nil {
		return 0, userErr
	}
	return userId, nil
}

// Delete book
func DeleteBook(w http.ResponseWriter, r *http.Request) {
	bid := mux.Vars(r)["bid"]
	bookId, idErr := getBookId(bid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := Model.Book{ID: int(bookId)}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

// Update book
func UpdateBook(w http.ResponseWriter, r *http.Request) {
	old_bid := mux.Vars(r)["bid"]
	old_bId, idErr := getBookId(old_bid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var stud Model.Book
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&stud); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid jason body")
		return
	}
	defer r.Body.Close()

	err := stud.Update(old_bId)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, stud)
}

// get all book
func GetAllBooks(w http.ResponseWriter, r *http.Request) {
	books, getErr := Model.GetAllBooks()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, books)
}
