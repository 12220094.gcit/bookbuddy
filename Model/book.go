package Model

import (
	"bookApp/data/postgres"
	"fmt"
)

// signup
type Admin struct {
	Fullname string
	Email    string
	Password string
}

const queryInsertAdmin = "INSERT into admin(fullname, email, password) VALUES ($1, $2, $3);"

func (adm *Admin) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin, adm.Fullname, adm.Email, adm.Password)
	return err
}

// login
type Login struct {
	Email    string
	Password string
}

const queryGetAdmin = "SELECT email, password FROM admin WHERE email = $1 and password = $2;"

func (adm *Login) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}

// add book
type Book struct {
	ID       int    `json:"bookid"`
	Title    string `json:"title"`
	Author   string `json:"author"`
	Cost     int    `json:"cost"`
	Quantity int    `json:"quantity"`
}

const queryInsertBook = "INSERT into book(bookid, title, author, cost, quantity) VALUES ($1, $2, $3, $4, $5);"

func (adm *Book) Create() error {
	_, err := postgres.Db.Exec(queryInsertBook, adm.ID, adm.Title, adm.Author, adm.Cost, adm.Quantity)
	return err
}

var queryGetBook = "SELECT bookid, title, author, cost, quantity FROM book WHERE bookid=$1;"

func (s *Book) Read() error {
	return postgres.Db.QueryRow(queryGetBook, s.ID).Scan(&s.ID, &s.Title, &s.Author, &s.Cost, &s.Quantity)
}

var queryUpdateBook = "UPDATE book SET bookid = $1, title = $2, author=$3, cost=$4, quantity = $5 WHERE bookid =$6;"

func (s *Book) Update(oldID int64) error {
	fmt.Println("TEST flow")
	_, err := postgres.Db.Exec(queryUpdateBook, s.ID, s.Title, s.Author, s.Cost, s.Quantity, oldID)
	return err
}

var queryDeleteBook = "DELETE FROM book WHERE bookid=$1;"

func (s *Book) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteBook, s.ID); err != nil {
		return err
	}
	return nil
}

func GetAllBooks() ([]Book, error) {
	rows, getErr := postgres.Db.Query("SELECT * FROM book;")
	if getErr != nil {
		return nil, getErr
	}
	books := []Book{}

	for rows.Next() {
		var s Book
		dbErr := rows.Scan(&s.ID, &s.Title, &s.Author, &s.Cost, &s.Quantity)
		if dbErr != nil {
			return nil, dbErr
		}
		books = append(books, s)
	}
	rows.Close()
	return books, nil
}
