package routes

import (
	"bookApp/controller"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")
	router.HandleFunc("/addbook", controller.Addbook).Methods("POST")

	router.HandleFunc("/updatebook/{bid}", controller.UpdateBook).Methods("PUT")
	router.HandleFunc("/deletebook/{bid}", controller.DeleteBook).Methods("DELETE")
	router.HandleFunc("/searchbook/{bid}", controller.GetBook).Methods("GET")
	router.HandleFunc("/books", controller.GetAllBooks)

	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 8081...")
	err := http.ListenAndServe(":8081", router)
	if err != nil {
		return
	}
}
