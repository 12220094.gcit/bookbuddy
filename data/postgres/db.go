package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	postgres_host     = "dpg-ci3mu1e4dad75mlc84rg-a.singapore-postgres.render.com"
	postgres_port     = 5432
	postgres_user     = "admin"
	postgres_password = "EKer0RcJvIkxnuBk7ix2WIPesgj90i46"
	postgres_dbname   = "my_db_9p11"
)

var Db *sql.DB

// init() is always called before main()
func init() {
	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)

	var err error

	Db, err = sql.Open("postgres", db_info)

	if err != nil {
		panic(err)
	} else {
		log.Println("Database successfully configured")
	}
}
